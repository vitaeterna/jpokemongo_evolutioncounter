package jPokemonGo_EvolutionCounter;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;

public class EvolutionCounter
{
    public static void main(String[] args) throws IOException
    {
        String[] lines = readFile("evolution.txt", Charset.defaultCharset()).split("\n");
        String[] split = null;

        String pokemonName = null;
        int bonbonsNeeded = 0;
        int bonbonsAvailable = 0;
        int pokemonsAvailable = 0;

        int tempCounter = 0;
        int counter = 0;
        int bonbonsMissing = 0;
        int pokemonsUnused = 0;
        int amount = 0;

        System.out.println("Silent's Evolution Calculator for Pokemon Go\n");

        for(String line : lines)
        {
            split = line.trim().split(" +");

            pokemonName = split[0];
            bonbonsNeeded = Integer.valueOf(split[1]);
            bonbonsAvailable = Integer.valueOf(split[2]);
            pokemonsAvailable = Integer.valueOf(split[3]);

            tempCounter = 0;
            counter = 0;
            bonbonsMissing = 0;

            tempCounter = bonbonsAvailable / bonbonsNeeded;

            while(tempCounter > 0)
            {
                bonbonsAvailable = bonbonsAvailable - tempCounter * bonbonsNeeded + tempCounter;
                counter += tempCounter;
                tempCounter = bonbonsAvailable / bonbonsNeeded;
            }

            bonbonsMissing = bonbonsNeeded - bonbonsAvailable;

            if(pokemonsAvailable < counter)
            {
                amount += pokemonsAvailable;

                System.out.println("You can evolve " + (String.valueOf(pokemonsAvailable).length() == 1 ? " " : "") + pokemonsAvailable + " " + pokemonName + ". You have too few " + pokemonName + ". Missing " + pokemonName + ": " + (counter - pokemonsAvailable));
            }
            else if(pokemonsAvailable == counter)
            {
                amount += counter;

                System.out.println("You can evolve " + (String.valueOf(counter).length() == 1 ? " " : "") + counter + " " + pokemonName + ". Perfect! (" + bonbonsAvailable + " bonbons left)");
            }
            else
            {
                amount += counter;

                System.out.println("You can evolve " + (String.valueOf(counter).length() == 1 ? " " : "") + counter + " " + pokemonName + ". You have too few bonbons. Missing bonbons: " + bonbonsMissing);

                pokemonsUnused = pokemonsAvailable - counter;

                if(pokemonsUnused >= bonbonsMissing)
                {
                    pokemonsUnused -= bonbonsMissing;

                    System.out.println("##### You should send " + bonbonsMissing + " " + pokemonName + " away! -> 1 more evolution #####");

                    tempCounter = 0;

                    if(pokemonsUnused >= bonbonsNeeded + 1)
                    {
                        pokemonsUnused = pokemonsUnused - (bonbonsNeeded + 1);

                        tempCounter = 1 + (pokemonsUnused / bonbonsNeeded);
                    }

                    if(tempCounter > 0)
                    {
                        System.out.println(" ##### You should send " + (tempCounter * bonbonsNeeded) + " " + pokemonName + " away! -> " + tempCounter + " more evolution #####");
                    }
                }
            }
        }

        System.out.println("               --");
        System.out.print("You can evolve " + (String.valueOf(amount).length() == 1 ? " " : "") + amount + " pokemons in total.");
    }

    static String readFile(String path, Charset encoding) throws IOException
    {
        byte[] encoded = Files.readAllBytes(Paths.get(path));
        return new String(encoded, encoding);
    }
}